# HTCondor cluster

This Helm chart deploys an HTCondor cluster on Kubernetes.

## Cluster pool status

View workers in pool:

```bash
$ kubectl exec -it -n htcondor htcondor-submit-0 -- bash -c 'condor_status'
```

## Submit a job

The job must be submitted as the `submituser` user.

```bash
$ kubectl exec -it -n htcondor htcondor-submit-0 -- bash -c ' \
    runuser submituser bash -c " \
    cd /tmp/sleep_test && condor_submit sleep.sub \
    "'

$ kubectl exec -it -n htcondor htcondor-submit-0 -- bash -c ' \
    runuser submituser bash -c " \
    condor_q \
    "'
```
