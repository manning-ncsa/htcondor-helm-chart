import logging
from time import sleep
import sys

logging.basicConfig(format='%(message)s')
log = logging.getLogger(__name__)
log.setLevel('DEBUG')
handler = logging.StreamHandler(sys.stdout)
handler.setLevel(logging.DEBUG)
log.addHandler(handler)

print('Sleeping 6 seconds...')
sleep(6)
print(f'''Python version: {sys.version}''')

