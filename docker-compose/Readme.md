# Docker Compose environment for local development

1. Create an HTCondor pool password file like so:

    ```bash
    openssl rand -hex 32 > volumes/htcondor/pool_password
    ```

2. Create the required Docker image archive files in `docker/images` as described in [`docker/images/Readme.md`](docker/images/Readme.md).

3. Launch the HTCondor cluster:

   ```bash
   docker compose up --build -d
   ```

4. Generate Pegasus workflow, create plan, and launch:

   ```bash
   docker exec -it -u submituser -w /home/submituser/pegasus pegasus bash -c '\
        python3 workflow_generator.py'
   docker exec -it -u submituser -w /home/submituser/pegasus pegasus bash -c '\
        pegasus-run /home/submituser/pegasus/submituser/pegasus/pipeline/run0001'
   ```

   Optionally you can watch the progress using:

   ```bash
   watch -n5 "docker exec -it -u submituser -w /home/submituser/pegasus pegasus bash -c '\
        pegasus-status -r submituser/pegasus/pipeline/run0001'"
   ```

5. Tear down the environment completely by running:

   ```bash
   $ docker compose down --remove-orphans --volumes 
   ```
