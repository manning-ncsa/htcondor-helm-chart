#!/bin/bash
set -euxo pipefail

rm -rf volumes/pegasus/submituser/ volumes/pegasus/cmf/{output,scratch}/
docker exec -it -u submituser -w /home/submituser/pegasus pegasus bash -c 'python3 workflow_generator.py'
docker exec -it -u submituser -w /home/submituser/pegasus pegasus bash -c 'pegasus-run submituser/pegasus/pipeline/run0001'
# watch -n5 "docker exec -it -u submituser -w /home/submituser/pegasus pegasus bash -c 'pegasus-status -r submituser/pegasus/pipeline/run0001'"
