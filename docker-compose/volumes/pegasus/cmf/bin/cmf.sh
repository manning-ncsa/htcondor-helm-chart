#!/bin/bash

set -euo pipefail

IMAGE=$1
shift
INPUT_FILE=$1
shift

mkdir -p input output

docker run --rm \
    -v $(readlink -f $INPUT_FILE):/opt/input/$(basename $INPUT_FILE) \
    -v $(pwd)/input:/opt/input \
    -v $(pwd)/output:/opt/output \
    $IMAGE \
    bash -c '\
        python3 yaml_preprocess.py && \
        cmf && \
        python3 clean_output.py && \
        python3 postprocess.py && \
        tar -czf /tmp/out.tgz -C /opt output && \
        mv /tmp/out.tgz /opt/output/out.tgz && \
        echo "Done"'
