#!/usr/bin/env python3
import os
import logging
from pathlib import Path
from argparse import ArgumentParser
from Pegasus.api import *

import logging
import sys
logging.basicConfig(format='%(message)s')
log = logging.getLogger(__name__)
log.setLevel('DEBUG')
handler = logging.StreamHandler(sys.stdout)
handler.setLevel(logging.DEBUG)
log.addHandler(handler)


class ProcessWorkflow:
    wf = None
    sc = None
    tc = None
    rc = None
    props = None

    dagfile = None
    wf_name = None
    wf_dir = None

    # --- Init ----------------------------------------------------------------
    def __init__(self, dagfile="workflow.yml"):
        self.dagfile = dagfile
        self.wf_name = "pipeline"
        self.wf_dir = str(Path(__file__).parent.resolve() / 'cmf')

    # --- Write files in directory --------------------------------------------
    def write(self):
        if not self.sc is None:
            self.sc.write()
        self.props.write()
        self.tc.write()
        self.rc.write()
        self.wf.write()

    # --- Configuration (Pegasus Properties) ----------------------------------
    def create_pegasus_properties(self):
        self.props = Properties()
        self.props["pegasus.transfer.worker.package"] = "false"
        self.props["pegasus.transfer.worker.package.autodownload"] = "false"
        self.props["pegasus.transfer.worker.package.strict"] = "false"
        # self.props["condor.request_cpus"] = 4
        # self.props["condor.request_memory"] = "2 GB"
        return

    # --- Site Catalog --------------------------------------------------------
    def create_sites_catalog(self, exec_site_name="condorpool"):
        self.sc = SiteCatalog()

        shared_scratch_dir = os.path.join(self.wf_dir, "scratch")
        local_storage_dir = os.path.join(self.wf_dir, "output")

        local = Site("local").add_directories(
            Directory(Directory.SHARED_SCRATCH, shared_scratch_dir).add_file_servers(
                FileServer("file://" + shared_scratch_dir, Operation.ALL)
            ),
            Directory(Directory.LOCAL_STORAGE, local_storage_dir).add_file_servers(
                FileServer("file://" + local_storage_dir, Operation.ALL)
            ),
        ).add_profiles(Namespace.ENV, key="DOCKER_HOST", value='unix:///shared/docker.sock')

        exec_site = (
            Site(exec_site_name)
            .add_pegasus_profile(style="condor")
            .add_condor_profile(universe="vanilla")
            .add_profiles(Namespace.PEGASUS, key="data.configuration", value="condorio")
            .add_profiles(Namespace.ENV, key="DOCKER_HOST", value='unix:///shared/docker.sock')
        )

        self.sc.add_sites(local, exec_site)

    def create_replica_catalog(self, exec_site_name="condorpool"):
        self.rc = ReplicaCatalog()
        self.rc.add_replica(
            exec_site_name,
            "config.yaml",
            os.path.join(self.wf_dir, "input/config.yml"),
        )
        self.rc.add_replica(
            exec_site_name,
            "./output/out.tgz",
            os.path.join(self.wf_dir, "output/out.tgz"),
        )

    # --- Transformation Catalog (Executables and Containers) -----------------
    def create_transformation_catalog(self, exec_site_name="condorpool"):
        self.tc = TransformationCatalog()
        cmf = Transformation(
            "cmf",
            site=exec_site_name,
            pfn=os.path.join(self.wf_dir, "bin/cmf.sh"),
            is_stageable=True,
        )

        self.tc.add_transformations(cmf)

    # --- Create Workflow -----------------------------------------------------
    def create_workflow(self):
        self.wf = Workflow(self.wf_name, infer_dependencies=True)

        cmf_config = File("config.yaml")
        cmf_out = File("cmf_out.txt")
        cmf_job = (
            Job("cmf").add_args("cmf:dev")
                .add_args(cmf_config)
                .add_inputs(cmf_config)
                .add_outputs(File("./output/out.tgz"), stage_out=True)
                .set_stdout(cmf_out, stage_out=True, register_replica=True)
        )

        self.wf.add_jobs(cmf_job)

    def plan(self):
        # write out the workflow to files, and graph it
        try:
            self.wf.write()
            self.wf.graph(include_files=True,  output="graph.png")
        except PegasusClientError as e:
            log.error(e)
        self.wf.plan()


if __name__ == "__main__":
    parser = ArgumentParser(description="Pegasus Process Workflow")

    parser.add_argument(
        "-s",
        "--skip_sites_catalog",
        action="store_true",
        help="Skip site catalog creation",
    )
    parser.add_argument(
        "-e",
        "--execution_site_name",
        metavar="STR",
        type=str,
        default="condorpool",
        help="Execution site name (default: condorpool)",
    )
    parser.add_argument(
        "-o",
        "--output",
        metavar="STR",
        type=str,
        default="workflow.yml",
        help="Output file (default: workflow.yml)",
    )

    args = parser.parse_args()

    workflow = ProcessWorkflow(args.output)

    if not args.skip_sites_catalog:
        print("Creating execution sites...")
        workflow.create_sites_catalog(args.execution_site_name)

    print("Creating workflow properties...")
    workflow.create_pegasus_properties()
    
    print("Creating transformation catalog...")
    workflow.create_transformation_catalog(args.execution_site_name)

    print("Creating replica catalog...")
    workflow.create_replica_catalog(args.execution_site_name)

    print("Creating process workflow dag...")
    workflow.create_workflow()

    workflow.write()
    workflow.plan()
