## Docker image archive files

Store Docker image archive files here. They will be loaded via `docker load` in the HTCondor worker containers.

### Example

Build the CMF image from the dev branch:

```bash
git clone -b dev git@gitlab.com:nsf-muses/module-cmf/cmf.git /tmp/cmf && cd /tmp/cmf
docker build . -t cmf:dev
docker save cmf:dev -o $CLONE_DIR/docker/images/cmf.dev.tar
```

where `CLONE_DIR` is the path to your clone of this git repo.
